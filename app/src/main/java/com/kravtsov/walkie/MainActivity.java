package com.kravtsov.walkie;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

import java.nio.ShortBuffer;

public class MainActivity extends AppCompatActivity {

    private MulticastSocket multiSocket;
    private InetAddress inetAddress;
    private String multicastAddress = "224.0.0.251";
    private int port = 5500;
    private Context context;

    private Boolean stop = true;
    private boolean self = true;
    private boolean stopServer = true;
    Server myServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        myServer = new Server();

        stop = true;
        startClient();

        findViewById(R.id.Button).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    stopServer = false;
                    self = false;
                    AudioRecorder(myServer);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    stopServer = true;
                    self = true;
                }
                return false;
            }
        });

    }

    private void startClient() {
        try {
            inetAddress = InetAddress.getByName(multicastAddress);
            multiSocket = new MulticastSocket(port);
            multiSocket.joinGroup(inetAddress);
            multiSocket.setBroadcast(true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                WifiManager wm = (WifiManager) getSystemService(context.WIFI_SERVICE);
                WifiManager.MulticastLock multicastLock = wm.createMulticastLock("mylock");

                multicastLock.acquire();

                byte[] buff = new byte[320];
                DatagramPacket packet = new DatagramPacket(buff, buff.length, inetAddress, port);
                int buffSize = AudioRecord.getMinBufferSize(8000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
                AudioTrack track = new AudioTrack(AudioManager.STREAM_MUSIC, 8000, AudioFormat.CHANNEL_OUT_MONO,
                        AudioFormat.ENCODING_PCM_16BIT, buffSize*10, AudioTrack.MODE_STREAM);

                while (stop) {
                    try {
                        multiSocket.receive(packet);
                        if(self) {
                            track.play();
                            byte[] ReciveByte = packet.getData();
                            short[] shorts = ByteToShort(ReciveByte);
                            track.write(shorts, 0, shorts.length);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                track.stop();
                track.release();
            }
        }).start();
    }

    public void AudioRecorder(final Server server){
        new Thread(new Runnable() {

            @Override
            public void run() {
                AudioRecord recorder;
                try {
                    int buffSize = AudioRecord.getMinBufferSize(8000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
                    recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, 8000, AudioFormat.CHANNEL_IN_MONO,
                            AudioFormat.ENCODING_PCM_16BIT, buffSize * 10);
                    recorder.startRecording();

                    short[] buffer = new short[160];
                    while (!stopServer) {
                        int n = recorder.read(buffer, 0, buffer.length);
                        server.ShowMessage(buffer);
                    }
                    recorder.stop();
                    recorder.release();

                } catch (Exception ex) {
                    ex.printStackTrace();
                    Log.d("LOG_TAG", "Running Audio error " + ex.toString());
                }
            }
        }).start();
    }

    short [] ByteToShort(byte [] input)
    {
        int iterations = input.length;
        ByteBuffer bb = ByteBuffer.wrap(input);
        ShortBuffer sb = bb.asShortBuffer( );

        short[] shortArray = new short[iterations/ 2];
        sb.get(shortArray);
        return shortArray;
    }


    @Override protected void onDestroy() {
        super.onDestroy();
        stop = false;
        stopServer = true;
    }
}