package com.kravtsov.walkie;


import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

/**
 * Created by User on 08.11.2015.
 */
public class Server {

    public Server()
    {
    }

    public void ShowMessage(short[] sound)
    {
        try{
            String multicastAddress = "224.0.0.251";
            MulticastSocket multiSocket;
            InetAddress inetAddress;
            int port = 5500;

            inetAddress = InetAddress.getByName(multicastAddress);
            multiSocket = new MulticastSocket(port);
            multiSocket.joinGroup(inetAddress);

            byte[] message = ShortToByte(sound);
            DatagramPacket dp = new DatagramPacket(message, message.length,inetAddress,port);
            multiSocket.send(dp);
        }
        catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private byte[] ShortToByte(short [] input)
    {
        int index;
        int iterations = input.length;
        ByteBuffer bb = ByteBuffer.allocate(input.length * 2);
        for(index = 0; index != iterations; ++index)
        {
            bb.putShort(input[index]);
        }
        return bb.array();
    }

}
